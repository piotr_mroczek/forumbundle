<?php

namespace PiotrMroczek\ForumComponentBundle;

use \InvalidArgumentException;

class NewThread
{
    protected $renderer;
    protected $view;
    protected $repository;

    protected $errors;
    protected $data;

    function __construct($repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param mixed $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return mixed
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    public function getRenderedView() {

        $renderer = $this->getRenderer();

        $v =  $renderer->render('new-thread.html.twig',
            [
                'errors' => $this->errors,
                'data'   => $this->data,
            ]
        );

        return $v;

    }


    public function create($name, $content, $nick) {

        $this->data = [

            'name'      => $name,
            'content'   => $content,
            'nick'      => $nick
        ];

        if (strlen($name) < 3 ) {

            $this->errors['name'] = 'Temat jest zbyt krótki';
        }

        if (strlen($name) > 100 ) {

            $this->errors['name'] = 'Temat jest zbyt długi';
        }

        if (strlen($content) < 3 ) {

            $this->errors['content'] = 'Treść jest zbyt krótka';
        }

        if (strlen($content) > 200 ) {

            $this->errors['content'] = 'Treść jest zbyt długa';
        }


        if (strlen($nick) < 3 ) {

            $this->errors['nick'] = 'Nick jest zbyt krótki';
        }

        if (strlen($nick) > 100 ) {

            $this->errors['nick'] = 'Nick jest zbyt długi';
        }

        if ($this->errors) {

            return false;
        }


        $this->repository->saveCreatedThread($this->data );

        return true;

    }

}