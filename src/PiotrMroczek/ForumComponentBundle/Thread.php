<?php

namespace PiotrMroczek\ForumComponentBundle;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;
use PiotrMroczek\ForumComponentBundle\Pagerfanta\View\Template\PaginatorTemplate;

class Thread
{
    protected $renderer;

    protected $page;
    protected $repository;

    protected $id;

    function __construct($id, $page, $repository)
    {
        $this->id = $id;
        $this->page = $page;
        $this->repository = $repository;
    }


    /**
     * @param mixed $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return mixed
     */
    public function getRenderer()
    {
        return $this->renderer;
    }


    public function getThread() {

        $respository = $this->repository;

        $thread = $respository->getThreadById($this->id);

        return $thread;

    }


    public function getRenderedView() {

        $v = $this->getThread();

        $adapter = new ArrayAdapter($v->getPosts() );
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setMaxPerPage(6); // 10 by default
        $maxPerPage = $pagerfanta->getMaxPerPage();

        $pagerfanta->setCurrentPage($this->page);

        $paginatorTpl = new PaginatorTemplate();

        $view = new DefaultView($paginatorTpl);
        $options = array('proximity' => 3);


        $id = $v->getId();
        $routeGenerator = function($page) use ($id ){

            $v = sprintf('/forum/temat/%d/%d', $id, $page);
            return $v;
        };

        $htmlPaginator = $view->render($pagerfanta, $routeGenerator, $options = []);

        $renderer = $this->getRenderer();

        $v =  $renderer->render('thread.html.twig',
            [
                'thread' => $v,
                'posts'       => $pagerfanta->getIterator(),
                'htmlPaginator' => $htmlPaginator,
            ]
        );

        return $v;
    }


}