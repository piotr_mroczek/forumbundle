;(function() {
  'use strict';







  /*------------------------------------*\
    filter each element check
  \*------------------------------------*/
  var filters_check = function() {

    var all = '[data-filter]';

    $('[data-filter-btn]').each(function(index, el) {

      var filtered = '[data-filter="' + $(this).attr('data-filter-btn') + '"]';

      if( $('[data-filter-btn]').hasClass('is-clicked') == false ) {
        $(all).show();
      }

      else if( $(this).hasClass('is-clicked') ) {
        $(filtered).show();
      }

      else {
        $(filtered).hide();
      }

    });

  }






  /*------------------------------------*\
    filter button
  \*------------------------------------*/
  $('[data-filter-btn]').click(function(event) {
    event.preventDefault();

    var all = '[data-filter]';
    var wrapper = '[data-filter-wrapper]';

    if( $(this).hasClass('is-clicked') == false ) {
      $(wrapper).fadeOut(function(){
        $(all).hide();
        filters_check();
        $(wrapper).fadeIn();
      })
      $(this).addClass('is-clicked');
    }

    else {
      $(wrapper).fadeOut(function(){
        filters_check();
        $(wrapper).fadeIn();
      })
      $(this).removeClass('is-clicked');
    }

  });






  /*------------------------------------*\
    email modal
  \*------------------------------------*/

  $('.article-topbox__btn--email').click(function(event) {
    event.preventDefault();
    event.stopPropagation();
    $('#email-url').fadeIn(300);
  });

  $('.email-modal').click(function(event) {
    event.stopPropagation();
  });

  $('html, body').click(function(event) {
    $('#email-url').fadeOut(300);
  });

  //a11y
  $(window).keyup(function(event) {
    if ( event.keyCode == 27 ) { //esc
      $('#email-url').fadeOut(300);
    }
  });



  /*------------------------------------*\
    print button
  \*------------------------------------*/
  $('.article-topbox__btn--print').click(function(event) {
    event.preventDefault();
    event.stopPropagation();
    window.print();
  });





  /*------------------------------------*\
    accordion
  \*------------------------------------*/
  $('.accordion__button').click(function(event) {
    event.preventDefault();

    var content = $(this).parent().find('.accordion__content');
    var content_height = content[0].scrollHeight;


    if( $(this).hasClass('is-clicked') == false ) {
      content.height(content_height);
      $(this).addClass('is-clicked');
    }

    else {
      content.height(0);
      $(this).removeClass('is-clicked');
    }



  });




  /*------------------------------------*\
    mobile-nav
  \*------------------------------------*/

  $('.mobile-nav__hamburger').click(function(event) {
    event.preventDefault();

    if( $(this).hasClass('is-clicked') == false ) {
      $(':root').css('overflow-y', 'hidden');
      $('.mobile-nav__window').fadeIn();
      $(this).addClass('is-clicked');
    }

    else {
      $(':root').css('overflow-y', 'auto');
      $('.mobile-nav__window').fadeOut();
      $(this).removeClass('is-clicked');
    }

  });


  /*------------------------------------*\
    slick
  \*------------------------------------*/

  if( $.fn.slick ) {

    $('.slider').slick({
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      prevArrow: '<button type="button" class="slick-prev">&nbsp;</button>',
      nextArrow: '<button type="button" class="slick-next">&nbsp;</button>'
    });

  }




})();
