<?php

namespace PiotrMroczek\ForumComponentBundle;

use PiotrMroczek\ForumComponentBundle\Model\Thread as ThreadModel;
use PiotrMroczek\ForumComponentBundle\Model\Post;

class ForumRepository
{

    protected $threads = [];

    function __construct()
    {
        $threads = [];

        $v = new ThreadModel();
        $v->setId(1);
        $v->setName('Pierwszy.');
        $v->setCreatedAt('28.03.2015');
        $threads[1] = $v;

        $v = new ThreadModel();
        $v->setId(2);
        $v->setName('Drugi.');
        $v->setCreatedAt('1.04.2015');
        $threads[2] = $v;

        $v = new ThreadModel();
        $v->setId(3);
        $v->setName('Trzeci.');
        $v->setCreatedAt('30.05.2015');
        $threads[3] = $v;

        $v = new ThreadModel();
        $v->setId(4);
        $v->setName('News z artykulu.');
        $v->setRelatedContent('someContent');
        $threads[4] = $v;

        $v = new ThreadModel();
        $v->setId(5);
        $v->setName('Piąty z 1 mock komentarzem.');
        $v->setPosts([1]);

        $threads[5] = $v;


        $v = new ThreadModel();
        $v->setId(6);
        $v->setName('Wątek z 1 komentarzem.');
        $v->setCreatedAt('28.03.2015');

            $post = new Post();
            $post->setCreatedAt('28.03.2015, 21.37');
            $post->setContent('Od 20 lat buduję praktykę, która powoduje, że pacjent NIE JEST ANONIMOWY. Znamy pacjentów, oni nas znają i oni znaja sie między sobą. Bo taki jest cel - poradnia ma być przyjazna. Chcecie fabryk? Dobrze. Tylko powiedzmy to sobie głośno jako społeczeństwo: od stycznia 2016 nie patrzymy sobie w oczy, nie podajemy reki, nie wymieniamy imion. No i proponuje jeszcze jeden warunek - lekarz też ma być anonimowy.');
            $post->setCreatedBy('jloretz');

        $v->setPosts([$post]);
        $threads[6] = $v;


        $v = new ThreadModel();
        $v->setId(7);
        $v->setName('Wątek z 2 komentarzami.');
        $v->setCreatedAt('28.03.2015');

            $post = new Post();
            $post->setCreatedAt('28.03.2015, 21.37');
            $post->setContent('Od 20 lat buduję praktykę, która powoduje, że pacjent NIE JEST ANONIMOWY. Znamy pacjentów, oni nas znają i oni znaja sie między sobą. Bo taki jest cel - poradnia ma być przyjazna. Chcecie fabryk? Dobrze. Tylko powiedzmy to sobie głośno jako społeczeństwo: od stycznia 2016 nie patrzymy sobie w oczy, nie podajemy reki, nie wymieniamy imion. No i proponuje jeszcze jeden warunek - lekarz też ma być anonimowy.');
            $post->setCreatedBy('jloretz');

            $post2 = new Post();
            $post2->setCreatedAt('1.06.2015, 10.37');
            $post2->setContent('Moja odpowiedz.');
            $post2->setCreatedBy('pmroczek');

        $v->setPosts([$post, $post2]);
        $threads[7] = $v;



        $v = new ThreadModel();
        $v->setId(8);
        $v->setName('Wątek z 16 komentarzami.');
        $v->setCreatedAt('28.03.2015');

            $post = new Post();
            $post->setCreatedAt('28.03.2015, 21.37');
            $post->setContent('Od 20 lat buduję praktykę, która powoduje, że pacjent NIE JEST ANONIMOWY. Znamy pacjentów, oni nas znają i oni znaja sie między sobą. Bo taki jest cel - poradnia ma być przyjazna. Chcecie fabryk? Dobrze. Tylko powiedzmy to sobie głośno jako społeczeństwo: od stycznia 2016 nie patrzymy sobie w oczy, nie podajemy reki, nie wymieniamy imion. No i proponuje jeszcze jeden warunek - lekarz też ma być anonimowy.');
            $post->setCreatedBy('jloretz');

            $post2 = new Post();
            $post2->setCreatedAt('1.06.2015, 10.37');
            $post2->setContent('Moja odpowiedz.');
            $post2->setCreatedBy('pmroczek');

        $v->setPosts([$post, $post2,$post, $post2,$post, $post2,$post, $post2,$post, $post2,$post, $post2,$post, $post2,$post, $post2,]);

        $threads[8] = $v;

        $this->threads = $threads;


    }


    public function getThreadById($id) {

        return $this->threads[$id];
    }

    public function getThreads() {

        return $this->threads;

    }



    public function saveCreatedThread($data) {

        //todo

    }

    public function saveCreatedPost($data) {

        //todo

    }


}

