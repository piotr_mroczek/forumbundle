<?php

namespace PiotrMroczek\ForumComponentBundle;

use PiotrMroczek\ForumComponentBundle\Model\Thread as ThreadModel;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;
use PiotrMroczek\ForumComponentBundle\Pagerfanta\View\Template\PaginatorTemplate;

class ThreadsList
{
    protected $threads;

    protected $renderer;
    protected $view;


    protected $page;
    protected $repository;


    function __construct($page, $repository)
    {
        $this->page         = $page;
        $this->repository   = $repository;
    }



    /**
     * @param mixed $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return mixed
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param mixed $threads
     */
    public function setThreads($threads)
    {
        $this->threads = $threads;
    }

    /**
     * @return mixed
     */
    public function getThreads()
    {
        $respository = $this->repository;

        $threads = $respository->getThreads();

        return $threads;
    }

    /**
     * @param mixed $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }


    public function getRenderedView()
    {

        $threads = $this->getThreads();

        $adapter = new ArrayAdapter($threads);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setMaxPerPage(6); // 10 by default
        $maxPerPage = $pagerfanta->getMaxPerPage();

        $pagerfanta->setCurrentPage($this->page);

        $paginatorTpl = new PaginatorTemplate();

        $view = new DefaultView($paginatorTpl);
        $options = array('proximity' => 3);

        $routeGenerator = function($page) {

            $v = sprintf('/forum/tematy/%d', $page);
            return $v;
        };

        $htmlPaginator = $view->render($pagerfanta, $routeGenerator, $options = []);

        $renderer = $this->getRenderer();

        $v =  $renderer->render('thread-list.html.twig',
            [
                'threads'       => $pagerfanta->getIterator(),
                'htmlPaginator' => $htmlPaginator,
            ]
        );

        return $v;

    }


}