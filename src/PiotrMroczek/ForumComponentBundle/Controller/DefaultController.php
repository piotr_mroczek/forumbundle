<?php

namespace PiotrMroczek\ForumComponentBundle\Controller;


use PiotrMroczek\ForumComponentBundle\ForumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiotrMroczek\ForumComponentBundle\ForumComponentFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function threadsListAction($page)
    {

       $repository      = new ForumRepository();
       $forumFactory    = new ForumComponentFactory($repository);

       $config = [];
       $forumComponent = $forumFactory->create($config);


       $list = $forumComponent->getThreadsList($page);

       return $this->render('ForumComponentBundle:Default:threadsList.html.twig',
           [
                'list' => $list
           ]
       );
    }

    public function threadAction($id, $page, Request $request)
    {
        $repository      = new ForumRepository();
        $forumFactory    = new ForumComponentFactory($repository);

        $config = [];
        $forumComponent = $forumFactory->create($config);

        $thread = $forumComponent->getThread($id, $page);



        $newPost = $forumComponent->createPost();

        if ($request->isMethod('POST')) {

            $threadId    = $request->get('thread_id');
            $content   = $request->get('content');
            $nick      = $request->get('nick');

            $r = $newPost->create($threadId, $content, $nick);

            if ($r) {

                return RedirectResponse::create('/forum/tematy');
            }

        }


        return $this->render('ForumComponentBundle:Default:thread.html.twig',

            [
                'thread'    => $thread,
                'newPost'   => $newPost
            ]

        );
    }


    public function createThreadAction(Request $request)
    {

        $repository      = new ForumRepository();
        $forumFactory    = new ForumComponentFactory($repository);

        $config = [];
        $forumComponent = $forumFactory->create($config);

        $newThread = $forumComponent->createThread();

        if ($request->isMethod('POST')) {

            $name = $request->get('name');
            $content = $request->get('content');
            $nick = $request->get('nick');


            $r = $newThread->create($name, $content, $nick);

            if ($r) {

                return RedirectResponse::create('/forum/tematy');
            }

        }



        return $this->render('ForumComponentBundle:Default:createthread.html.twig',

            [
                'newThread' => $newThread
            ]

        );
    }

}
