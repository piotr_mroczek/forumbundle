<?php

namespace PiotrMroczek\ForumComponentBundle;

use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_SimpleFunction;

class ForumComponent
{
    protected $repository;

    function __construct($repository)
    {
        $this->repository = $repository;
    }


    protected function injectRenederer($v) {

        $templatesPath = sprintf('%s/Resources/templates', __DIR__);

        $loader = new Twig_Loader_Filesystem($templatesPath);

        $twig = new Twig_Environment($loader);

        $function = new Twig_SimpleFunction('route', function ($routeName, $params = null) {

            if ('thread' === $routeName) {

                $v = sprintf('/forum/temat/%d', $params['id']);

                return $v;
            }

            if ('regulamin' === $routeName) {

                $v = '/forum/regulamin';

                return $v;
            }

            if ('pomoc' === $routeName) {

                $v = '/forum/pomoc';

                return $v;
            }

            if ('nowy-temat' === $routeName) {

                $v = '/forum/nowy-temat';

                return $v;
            }

        });

        $twig->addFunction($function);


        $v->setRenderer($twig);
    }

    public function getThreadsList($page) {

        $v = new ThreadsList($page, $this->repository);

        $this->injectRenederer($v);

        return $v;

    }

    public function getThread($id, $page) {

        $v = new Thread($id, $page, $this->repository);

        $this->injectRenederer($v);

        return $v;

    }


    public function createThread() {

        $v = new NewThread($this->repository);

        $this->injectRenederer($v);

        return $v;

    }


    public function createPost() {

        $v = new NewPost($this->repository);

        $this->injectRenederer($v);

        return $v;

    }



}