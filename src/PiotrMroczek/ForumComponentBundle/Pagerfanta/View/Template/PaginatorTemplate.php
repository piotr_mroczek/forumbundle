<?php



namespace PiotrMroczek\ForumComponentBundle\Pagerfanta\View\Template;

use Pagerfanta\View\Template\Template;

/**
 * @author Pablo Díez <pablodip@gmail.com>
 */
class PaginatorTemplate extends Template
{
    static protected $defaultOptions = array(

        'previous_message'   => '<a href="" class="pagination__arrow-left">&nbsp;</a>',
        'next_message'       => '<a href="" class="pagination__arrow-right">&nbsp;</a>',

        'css_disabled_class' => '',
        'css_dots_class'     => 'pagination__page',
        'css_current_class'  => 'pagination__page pagination__page--current',
        'dots_text'          => '...',

        'container_template' => '<div class="pagination">%pages%</div>',
        'page_template'      => '<a href="%href%" class="pagination__page" data="page_template">%text%</a>',

        'span_template'      => '<a class="%class%" data="span_template">%text%</a>',
    );

    public function container()
    {
        return $this->option('container_template');
    }

    public function page($page)
    {
        $text = $page;

        return $this->pageWithText($page, $text);
    }

    public function pageWithText($page, $text)
    {
        $search = array('%href%', '%text%');

        $href = $this->generateRoute($page);
        $replace = array($href, $text);

        return str_replace($search, $replace, $this->option('page_template'));
    }


    public function previousDisabled()
    {
        return false;
    }

    public function previousEnabled($page)
    {
        $href = $this->generateRoute($page);
        return sprintf( '<a href="%s" class="pagination__arrow-left">&nbsp;</a>', $href);
    }

    public function nextDisabled()
    {
        return false;
    }

    public function nextEnabled($page)
    {
        $href = $this->generateRoute($page);
        return sprintf( '<a href="%s" class="pagination__arrow-right">&nbsp;</a>', $href);

    }

    public function first()
    {
        return $this->page(1);
    }

    public function last($page)
    {
        return $this->page($page);
    }

    public function current($page)
    {
        return $this->generateSpan($this->option('css_current_class'), $page);
    }

    public function separator()
    {
        return $this->generateSpan($this->option('css_dots_class'), $this->option('dots_text'));
    }

    private function generateSpan($class, $page)
    {
        $search = array('%class%', '%text%');
        $replace = array($class, $page);

        return str_replace($search, $replace, $this->option('span_template'));
    }
}
