<?php

namespace PiotrMroczek\ForumComponentBundle\Model;


class Thread
{
    protected $id;
    protected $name;
    protected $createdAt;
    protected $posts = [];

    protected $relatedContent;

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $relatedContent
     */
    public function setRelatedContent($relatedContent)
    {
        $this->relatedContent = $relatedContent;
    }

    /**
     * @return mixed
     */
    public function getRelatedContent()
    {
        return $this->relatedContent;
    }


    public function hasRelatedContent() {

        return (false === is_null($this->relatedContent) );
    }


}