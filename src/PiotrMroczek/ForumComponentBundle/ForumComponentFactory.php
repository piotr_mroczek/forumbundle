<?php

namespace PiotrMroczek\ForumComponentBundle;



class ForumComponentFactory
{
    protected $repository;

    function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function create($config)
    {
        $v = new ForumComponent($this->repository);

        return $v;
    }

}

