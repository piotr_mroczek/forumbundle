<?php

namespace PiotrMroczek\ForumComponentBundle;

use \InvalidArgumentException;

class NewPost
{
    protected $renderer;
    protected $view;
    protected $repository;

    protected $errors;
    protected $data;

    function __construct($repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param mixed $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return mixed
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    public function getRenderedView() {

        $renderer = $this->getRenderer();

        $v =  $renderer->render('new-post.html.twig',
            [
                'errors' => $this->errors,
                'data'   => $this->data,
            ]
        );

        return $v;

    }


    public function create($threadId, $content, $nick) {

        $this->data = [

            'thread_id'      => $threadId,
            'content'   => $content,
            'nick'      => $nick
        ];


        if (strlen($content) < 3 ) {

            $this->errors['content'] = 'Treść jest zbyt krótka';
        }

        if (strlen($content) > 200 ) {

            $this->errors['content'] = 'Treść jest zbyt długa';
        }

       if (strlen($nick) < 3 ) {

            $this->errors['nick'] = 'Nick jest zbyt krótki';
        }

        if (strlen($nick) > 100 ) {

            $this->errors['nick'] = 'Nick jest zbyt długi';
        }

        if ($this->errors) {

            return false;
        }

        $this->repository->saveCreatedPost($this->data );

        return true;

    }

}